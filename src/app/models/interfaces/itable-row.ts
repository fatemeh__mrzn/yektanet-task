
/**
 * Table Row Interface
 */
export interface ITableRow {
  id: number;
  name: string;
  date: string;
  title: string;
  field: string;
  old_value: any;
  new_value: any;
}

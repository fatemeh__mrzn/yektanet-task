import {Component, OnInit, ViewChild} from '@angular/core';
import {ITableRow} from '../../models/interfaces/itable-row';
import SampleData from '../../../assets/data/data.json';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

  private data: ITableRow[] = SampleData as ITableRow[];

  public currentDataChunk: ITableRow[] = [];

  public currentPage: number;
  public rowsPerPage: number;
  public totalPages: number;

  public tableHeaders: ITableRow;

  constructor() {
    this.currentPage = 1;
    this.rowsPerPage = 50;
    this.totalPages = Math.ceil(this.data.length / this.rowsPerPage);
    this.getCurrentDataChunk();

    this.tableHeaders = {
      id: 0,
      name: 'نام',
      date: 'تاریخ',
      title: 'عنوان',
      field: 'فیلد',
      old_value: 'مقدار قبلی',
      new_value: 'مقدار جدید'
    };
  }

  ngOnInit(): void {
  }

  getCurrentDataChunk(): void {
    const start = (this.currentPage - 1) * this.rowsPerPage;
    const end = (this.currentPage * this.rowsPerPage);
    this.currentDataChunk = this.data.slice(start, end);
  }

  changeCurrentPage(newPage: number): void {
    console.log('Current: ' + this.currentPage + ' | New: ' + newPage);
    if (newPage !== this.currentPage && newPage > 0 && newPage <= this.totalPages) {
      this.currentPage = newPage;
      console.log('New Current: ' + this.currentPage);
      this.getCurrentDataChunk();
    }
  }
}
